var Main = {}; //construimos el objeto main que va a a ejecutar todas las instrucciones
var player; // declaramos el objeto player que recibe el rectangulo donde se va a reproducir el video

var tizenId = tizen.application.getCurrentApplication().appInfo.packageId;//obteneemos el id del monitor
var url = "file:///opt/usr/apps/"+tizenId +"/res/wgt/media/bank.mp4"; //la direccion donde esta el video en la memoria del monitor

Main.onLoad = function (){
    "use strict";
     if (window.tizen === undefined) {
    	 console.log('This application needs to be run on Tizen device');
     }else{
        setTimeout(function () {
            console.log("La aplicacion se ha iniciado");
            Main.initplayer();// se debe iniciar el player
        }, 5000)         
     }
};

Main.initplayer = function(){
    //"use strict";
    var divLogo = document.getElementById("divLogo");
    divLogo.style.display = "none";
    console.log("Player iniciado");
    player = document.getElementById('av-player');
    Main.play(url);
    Main.statusUSB();
};
Main.statusUSB = function(){
	var watchID;
	
	function onStorageStateChanged(storage){
		if(storage.type === "EXTERNAL" && storage.state === "MOUNTED"){
			Main.readFile();
		}
		else if(storage.type === "EXTERNAL" && storage.state === "REMOVED"){
			console.log("USB desconectado");
		}
		else{}
	}
	watchID = tizen.filesystem.addStorageStateChangeListener(onStorageStateChanged);
	console.log(watchID);
}


Main.play = function(url) {
    "use strict";
    // Esta estructura de listener se copia y pega tal cual en el codigo asi como viene en la pagina
    var listener = {
            onbufferingstart: function() {
                console.log("Buffering start.");
            },
            onbufferingprogress: function(percent) {
                console.log("Buffering progress percent : " + percent);
            },
            onbufferingcomplete: function() {
                console.log("Buffering complete.");
            },
            oncurrentplaytime: function(currentTime) {
                console.log("Current Playtime : " + currentTime);
            },
            onevent: function(eventType, eventData) {
                console.log("event type error : " + eventType + ", data: " + eventData);
            },
            onerror: function(eventType) {
                console.log("event type error : " + eventType);
            },
            onstreamcompleted: function() {
                console.log("Stream Completed");
                webapis.avplay.stop();
                Main.play(url);
            }
        };

    console.log('open: '+url);
    console.log('open status: '+ webapis.avplay.open(url));
    console.log('setListener status: '+ webapis.avplay.setListener(listener));
    console.log('prepare status: '+ webapis.avplay.prepare());
    webapis.avplay.setDisplayRect(0, 0, 1700, 1080); //DEfinimos el tamaño del video o lo recortamos
    //webapis.avplay.setDisplayRotation("PLAYER_DISPLAY_ROTATION_90") // ajustamos la rotacion del video
    console.log('play status: '+webapis.avplay.play());
};



Main.readFile = function(){
	$("#table").children().remove();
	function onsuccess(files) {
		for (var i = 0; i < files.length; i++) {
	    	if (files[i].name == 'result.txt')
	     		files[i].readAsText(
	     			function(str) {
	     				console.log("The file content " + str);
	     				$("#table").append(str);
	     			}, 
	     			function(e) {
	     				console.log("Error " + e.message);
	     			}, 
	     			"UTF-8"
	     		);
	    }

	}
	function onerror(error) {
		console.log("The error " + error.message + " occurred when listing the files in the selected folder");
	}
	var documentsDir;
	tizen.filesystem.resolve(
	    'removable_sda1',
	     function(dir) {
	     	documentsDir = dir;
	     	dir.listFiles(onsuccess,onerror);
	     }, 
	     function(e) {
	     	console.log("Error" + e.message);
	     }, 
	     "rw"
	);
}